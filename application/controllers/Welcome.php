<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://data.bmkg.go.id/DataMKG/TEWS/gempadirasakan.json',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		));
		// string
		$response = curl_exec($curl);
		curl_close($curl);

		// to array
		$response = json_decode($response, true);
		$data = [
			"gempa" => $response['Infogempa']['gempa']
		];
		$this->load->view('welcome_message', $data);
	}
}
